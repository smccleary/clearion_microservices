import 'dart:convert';
import 'dart:io';

class Fixtures {
  static String NS_ROLES_URL = 'https://rest.netsuite.com/rest/roles';
}

class Utilities {
  /// performs a simple get operation that returns a future [List]
  static Future<List<dynamic>> onGetList({String url, Map<String, String> headers}) async {
    // initialize an http client for our requests
    HttpClient client = new HttpClient();

    // for low level command line apps, we must use the new Utf8Decoder() class
    const utf8 = const Utf8Decoder();

    // perform the actual get call
    return client.getUrl(Uri.parse(url)).then((HttpClientRequest req) {
      // iterate the user-defined headers and add to the request
      if (headers != null) headers.forEach((key, val) => req.headers.add(key, val));

      // close the request obj after we are done modding it
      return req.close();
    }).then((HttpClientResponse res) async => json.decode(await res.transform(Utf8Decoder()).join()));
  }

  /// gets the user's roles based on [email] and [pass] supplied. returns `null` if the email
  /// of password was not supplied rather than throwing a nested error
  static Future<List<Role>> onGetRoles({String email, String pass}) async {
    if (email == null || pass == null) return null;
    var roles = await onGetList(
      url: Fixtures.NS_ROLES_URL,
      headers: {
        'Authorization': 'NLAuth nlauth_email=$email, nlauth_signature=$pass',
      },
    );
    return roles.map((j) => new Role.fromJson(j)).toList();
  }
}

class Account {
  String internalId;
  String name;
  String type;

  Account({
    this.internalId,
    this.name,
    this.type,
  });

  factory Account.fromJson(Map<String, dynamic> json) => new Account(
        internalId: json["internalId"],
        name: json["name"],
        type: json["type"],
      );

  Map<String, dynamic> toJson() => {
        "internalId": internalId,
        "name": name,
        "type": type,
      };
}

Role roleFromJson(String str) => Role.fromJson(json.decode(str));

String roleToJson(Role data) => json.encode(data.toJson());

class Role {
  Account account;
  RoleInfo role;
  DataCenterUrLs dataCenterUrLs;

  Role({
    this.account,
    this.role,
    this.dataCenterUrLs,
  });

  factory Role.fromJson(Map<String, dynamic> json) => new Role(
        account: Account.fromJson(json["account"]),
        role: RoleInfo.fromJson(json["role"]),
        dataCenterUrLs: DataCenterUrLs.fromJson(json["dataCenterURLs"]),
      );

  Map<String, dynamic> toJson() => {
        "account": account.toJson(),
        "role": role.toJson(),
        "dataCenterURLs": dataCenterUrLs.toJson(),
      };
}

class DataCenterUrLs {
  String webservicesDomain;
  String restDomain;
  String systemDomain;

  DataCenterUrLs({
    this.webservicesDomain,
    this.restDomain,
    this.systemDomain,
  });

  factory DataCenterUrLs.fromJson(Map<String, dynamic> json) => new DataCenterUrLs(
        webservicesDomain: json["webservicesDomain"],
        restDomain: json["restDomain"],
        systemDomain: json["systemDomain"],
      );

  Map<String, dynamic> toJson() => {
        "webservicesDomain": webservicesDomain,
        "restDomain": restDomain,
        "systemDomain": systemDomain,
      };
}

class RoleInfo {
  int internalId;
  String name;

  RoleInfo({
    this.internalId,
    this.name,
  });

  factory RoleInfo.fromJson(Map<String, dynamic> json) => new RoleInfo(
        internalId: json["internalId"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "internalId": internalId,
        "name": name,
      };
}
