import 'dart:convert';
import 'dart:html';

import 'package:uuid/uuid.dart';

class NetsuiteCLI {
  Map<String, String> _headers;
  String _url = 'https://3737768-sb1.restlets.api.netsuite.com/app/site/hosting/restlet.nl?script=825&deploy=1';

  NetsuiteCLI({String email, String pwd, String acct, String role}) {
    this._headers = {
      'Authorization': 'NLAuth nlauth_account=$acct, nlauth_email=$email, nlauth_signature=$pwd, nlauth_role=$role',
      'Content-Type': 'application/json',
    };
  }

  bool connect() {
    return false;
  }

  void send(String service, Map args) {
    // todo: catch errors
    HttpRequest.request(this._url, method: 'POST', sendData: json.encode(args), requestHeaders: this._headers)
        .then((resp) {
      print(resp.responseText);
    });
  }
}

class ZPLCoords {
  int y;
  int x;
  int size;

  ZPLCoords({this.y, this.x, this.size});
}

class ZPLEntity implements ZPLCoords {
  String type;
  String text;
  int height;
  int width;
  int density;

  @override
  int size;

  @override
  int x;

  @override
  int y;

  String _id;

  ZPLEntity(
    this.type,
    this.text, {
    this.height,
    this.width,
    this.x,
    this.y,
    this.size,
    this.density,
  }) {
    this._id = new Uuid().v1();
  }

  @override
  String toString() {
    return '\n{id: $_id, type: $type, x: $x, y: $y}';
  }
}

class ZPLMessage {
  int _defaultLeading = 40;
  int _nextY = 0;
  int _offsetIncr = 20;
  ZPLCoords initialCoords;
  String _msg;
  List<ZPLEntity> _entities;

  ZPLMessage({int x = 20, int y = 20, int initialSize = 30}) {
    this._msg = '';
    this._entities = [];
    this.initialCoords = new ZPLCoords(x: x, y: y, size: initialSize);
  }

  List<ZPLEntity> getEntities() => this._entities;

  void close() => this._msg = '^XA\n${this._msg}^XZ';

  ZPLMessage text(String text, {int size, int x, int y}) {
    // apply the default leading to the coord.y
    this._nextY += this._defaultLeading;

    // check if the x, y position is overridden for this block
    final String xPos = x != null ? x.toString() : this.initialCoords.x.toString();
    final String yPos = y != null ? y.toString() : this._nextY.toString();

    // temporary cast so we can perform leading ops below, then return the class instance
    final result = this.setSize(size ?? this.initialCoords.size, ['^FO${xPos},${yPos}^FD$text^FS']);

    // register the entity
    this._entities.add(new ZPLEntity(
          'text',
          text,
          size: size ?? this.initialCoords.size,
          x: int.parse(xPos),
          y: int.parse(yPos),
        ));

    // make sure to offset the leading in the event the text size was overridden
    var offset = size != null ? size - this._defaultLeading : 0;
    if (offset > 0) this._nextY += offset + this._offsetIncr;

    return result;
  }

  ZPLMessage barcode(String text, {String density = '5', int height = 150, int x, int y}) {
    this._nextY += this._defaultLeading;

    // check if the x, y position is overridden for this block
    final String xPos = x != null ? x.toString() : this.initialCoords.x.toString();
    final String yPos = y != null ? y.toString() : this._nextY.toString();

    // register the entity
    this._entities.add(new ZPLEntity(
          'barcode',
          text,
          x: int.parse(xPos),
          y: int.parse(yPos),
          height: height,
          density: int.parse(density),
        ));

    this._msg += '^BY$density,2,${height.toString()}^FO${xPos},${yPos}^BC^FD$text^FS\n';
    return this;
  }

  ZPLMessage setSize(int size, List<String> zpls) {
    final int minSize = (size / 2).round();
    final String str = zpls.join();
    this._msg += '^CFA,${size.toString()}$str^CFA,${minSize.toString()}\n';
    return this;
  }

  String render() => this._msg;

  ZPLMessage divider({int width = 700, int height = 3}) {
    this._nextY += this._defaultLeading;

    // register the entity
    this._entities.add(new ZPLEntity(
          'divider',
          null,
          x: this.initialCoords.x,
          y: this._nextY,
          height: height,
          width: width,
        ));

    this._msg += '^FO${this.initialCoords.x},${this._nextY}^GB${width.toString()},1,${height.toString()}^FS\n';
    return this;
  }
}
