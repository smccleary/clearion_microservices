import 'package:clearion_microservices/clearion_microservices.dart';

main(List<String> arguments) {
  // initialize a new zpl message creator
  ZPLMessage zplCreator = new ZPLMessage(x: 20, y: 20, initialSize: 30);

  // compose a sample label
  zplCreator
      .text('1 Line')
      .text('60pt Line', size: 60)
      .text('40pt Line', size: 40)
      .divider()
      .text('4 Line')
      .text('55 Line')
      .divider(height: 1, width: 200)
      .text('small 15pt line', size: 15)
      .divider()
      .barcode('1903730', height: 80)
      .close();

  // get the actual zpl contents
  String zpl = zplCreator.render();

  print('Sample Output:');
  print(zplCreator.getEntities());
  print(zpl);
}
