import 'dart:io';

import 'package:args/args.dart';
import 'package:clearion_microservices/classes.dart';

ArgResults argResults;

// stdout.writeln('Type something');
// String input = stdin.readLineSync();
// stdout.writeln('You typed: $input');

// pub global activate --source path .
// pub global deactivate <package>

// String email, String pwd, String acct, String role
class CLITester {
  static process(String mode, [String input]) async {
    // default the echo mode to true in the event that is has been reset
    stdin.echoMode = true;
    switch (mode) {
      case 'test':
        List<Role> roles = await Utilities.onGetRoles(email: 'smccleary@clearion.com', pass: ';NgRXT426!');
        var str = 'Which role do you want to use?\n', inc = 1;
        roles.forEach((role) {
          str += '  ${inc.toString()}.\t${role.role.name} (${role.account.internalId})\n';
          inc++;
        });
        stdout.writeln(str);
        break;
      case 'email':
        // todo: we have nothing
        stdout.writeln('Enter your email address:');
        CLITester.process('sig', stdin.readLineSync());
        break;
      case 'sig':
        // turn echo mode off so the password is not displayed in the console
        stdin.echoMode = false;
        stdout.writeln('Ok $input, what is your password:');
        CLITester.process('acct', stdin.readLineSync());
        break;
      case 'acct':
        // todo: we have password
        stdout.writeln('Do you know your account #?');
        CLITester.process('role', stdin.readLineSync());
        break;
      case 'role':
        // todo: we have acct
        stdout.writeln('What role are you logging in as? Enter a number from the list below:');

        // todo: query the roles endpoint to populate this list
        stdout.writeln('>  1: Administrator');
        stdout.writeln('>  2: clearion IT Manager');
        stdout.writeln('>  3: clearion Website Manager');

        CLITester.process('done', stdin.readLineSync());
        break;
      case 'done':
        // todo: we have role
        stdout.writeln(
            'Thanks! You are now logged into the clearion command line interface. Type --help to see a list of the avaliable commands');
        break;
    }
  }
}

void main(List<String> arguments) {
  final ArgParser argParser = new ArgParser()
    ..addOption('name', abbr: 'n', defaultsTo: 'World', help: "Who would you like to greet?")
    ..addFlag('help', abbr: 'h', negatable: false, help: "Displays this help information.");

  argResults = argParser.parse(arguments);

  final String name = argResults['name'];
  if (argResults['help']) {
    print("""
** clearion CLI Help **
${argParser.usage}
    """);
  } else {
    CLITester.process('test');
  }
}
